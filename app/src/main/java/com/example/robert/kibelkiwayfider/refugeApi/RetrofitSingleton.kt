package com.example.robert.kibelkiwayfider.refugeApi

import com.squareup.moshi.Moshi
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

object RetrofitSingleton{
val retrofit = Retrofit.Builder()
        .baseUrl("https://www.refugerestrooms.org/api/")
        .addConverterFactory(MoshiConverterFactory.create(Moshi.Builder().add(com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory()).build()))
        .build()
val apiServices=retrofit.create(ApiServices::class.java)
}