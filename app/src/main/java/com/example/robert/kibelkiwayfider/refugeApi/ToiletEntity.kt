package com.example.robert.kibelkiwayfider.refugeApi

data class ToiletEntity(var name: String, var accessible: Boolean, var unisex: Boolean,
                        var directions: String, var comment: String,
                        var latitude: Float, var longitude: Float,
                        var changing_table: Boolean)