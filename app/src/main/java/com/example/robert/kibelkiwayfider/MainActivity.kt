package com.example.robert.kibelkiwayfider

import android.Manifest
import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.os.PersistableBundle
import android.support.design.widget.BottomSheetBehavior
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.AppCompatImageButton
import android.util.Log
import android.util.TypedValue
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.example.robert.kibelkiwayfider.refugeApi.RetrofitSingleton
import com.example.robert.kibelkiwayfider.refugeApi.ToiletEntity
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.tasks.Task
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MainActivity : AppCompatActivity(), OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener //, FragmentActivity()
{

    companion object {
        private const val REQUEST_CODE_LOCATION = 1
        private const val REQUEST_CHECK_SETTINGS = 2
    }

    private val apiServices = RetrofitSingleton.apiServices
    //private var wasntClicked: Boolean = true
    private var wasZoomedIn : Boolean = false
    private var isMapReady: Boolean = false
    private var map: GoogleMap? = null
    // private var nearbyToilets: List<ToiletEntity.kt>? = emptyList()
    // private var searchToilets: List<ToiletEntity.kt>? = emptyList()
    private var latitude: Double? = null
    private var longitude: Double? = null
    private var markerData: MutableMap<Marker, ToiletEntity>? = mutableMapOf()
    //private var locations
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private val locationRequest = LocationRequest().apply {
        interval = 3000
        fastestInterval = 3000
        priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
    }
    lateinit var builder: LocationSettingsRequest.Builder
    private lateinit var locationCallback: LocationCallback
    private var isUpdatingLocation: Boolean = false
    //private lateinit var binding: FragmentMapBinding
    private lateinit var bottomSheetBehavior: BottomSheetBehavior<View>
    private lateinit var searchButton: AppCompatImageButton


    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d("TAG", (latitude == null).toString())
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        if (ContextCompat.checkSelfPermission(this@MainActivity, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            askForPemissionToAccessLocation()
        } else {
            loadNearbyToilets()
        }
        //val check = findViewById<View>(R.id.activity_main_bottom_sheet)
        (supportFragmentManager.findFragmentById(R.id.map_display) as? SupportMapFragment)?.getMapAsync(this)
        searchButton = findViewById(R.id.searchbar_button)
        setUpBottomSheet()
        //setUpEditText()
        //   animateButton()
        setUpLocationUpdates()
        val <View> searchbar = (findViewById(R.id.searchbar_layout)) as View
        searchbar.clipToOutline = true
        // binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
    }

    override fun onResume() {
        super.onResume()
        startLocationUpdates()
    }

    private fun askForPemissionToAccessLocation() {

        ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), REQUEST_CODE_LOCATION)

        return
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_CODE_LOCATION -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    //what to do with permission
                    val mapFragment = supportFragmentManager
                            .findFragmentById(R.id.map_display) as SupportMapFragment
                    mapFragment.getMapAsync(this)
                }
            }
        }
    }

//    fun setUpEditText() {
//        val editText = this.findViewById<EditText>(R.id.activity_main_searchbar_text_edit)
//        editText.setText(this.getString(R.string.activity_main_text_edit_default_value))
//        editText.isClickable = true
//        editText.setOnClickListener {
//            if (wasntClicked) {
//                editText.setText("")
//                wasntClicked = false
//            }
//        }
//    }

//    fun animateButton() {
//        val animation = this.findViewById<Button>(R.id.activity_main_search_button).background as AnimationDrawable
//        animation.alpha = 255
//        animation.setEnterFadeDuration(500)
//        animation.setExitFadeDuration(500)
//        animation.start()
//    }

    private fun setUpBottomSheet() {
        bottomSheetBehavior = BottomSheetBehavior.from(findViewById<View>(R.id.bottom_sheet))
        findViewById<AppCompatImageButton>(R.id.searchbar_button).setOnClickListener {
            searchForToilets(findViewById<EditText>(R.id.searchbar_text_edit).text.toString())
        }
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN

        findViewById<View>(R.id.bottom_sheet).visibility = View.GONE

    }

    override fun onMapReady(input: GoogleMap) {
        map = input
        isMapReady = true
        map?.setOnInfoWindowClickListener(this)
        //if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
        //    map?.isMyLocationEnabled = true
        //}
    }

    fun loadNearbyToilets() {

        if (ContextCompat.checkSelfPermission(this@MainActivity, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            fusedLocationClient.lastLocation.addOnSuccessListener { location: Location? ->
                latitude = location?.latitude
                longitude = location?.longitude
                //todo
                if (latitude != null && longitude != null) {
                    apiServices.getNearby(latitude!!.toFloat(), longitude!!.toFloat()).enqueue(object : Callback<List<ToiletEntity>> {
                        override fun onFailure(call: Call<List<ToiletEntity>>, t: Throwable) {
                            val bar = Snackbar.make(findViewById(R.id.activity_main_coordinator_layout),
                                    getString(R.string.activity_main_toilets_snackbar_message), Snackbar.LENGTH_INDEFINITE)
                            bar.show()
                            bar.setAction(getString(R.string.activity_main_snackbar_confirmation)) { reloadNearby() }
                        }

                        override fun onResponse(call: Call<List<ToiletEntity>>, response: Response<List<ToiletEntity>>?) {
                            if (isMapReady) {
                                setUpMap(map)
                                //todo use optionals
                                if (response?.body() != null) {
                                    addMarkersToMap(response.body()!!, map)
                                }
                            } else {
                                reloadNearby()
                            }
                        }
                    })
                } else {
                    loadNearbyToilets()
                }
            }
            fusedLocationClient.lastLocation.addOnFailureListener {
                Log.d("TAG", "Nearby toilets data load failed")
                val bar = Snackbar.make(findViewById(R.id.activity_main_coordinator_layout),
                        getString(R.string.activity_main_location_snackbar_message), Snackbar.LENGTH_INDEFINITE)
                bar.show()
                bar.setAction(getString(R.string.activity_main_snackbar_confirmation)) { loadNearbyToilets() }
            }
        }
    }

    private fun reloadNearby() {
        apiServices.getNearby(latitude!!.toFloat(), longitude!!.toFloat()).enqueue(object : Callback<List<ToiletEntity>> {
            override fun onFailure(call: Call<List<ToiletEntity>>, t: Throwable) {
                val bar = Snackbar.make(findViewById(R.id.activity_main_coordinator_layout),
                        getString(R.string.activity_main_toilets_snackbar_message), Snackbar.LENGTH_INDEFINITE)
                bar.show()
                bar.setAction(getString(R.string.activity_main_snackbar_confirmation)) { reloadNearby() }
            }

            override fun onResponse(call: Call<List<ToiletEntity>>, response: Response<List<ToiletEntity>>) {
                //nearbyToilets = response.body()!!
                if (isMapReady) {
                    //todo use optionals
                    if (response.body() != null) {
                        addMarkersToMap(response.body()!!, map)
                    }
                } else {
                    reloadNearby()
                }
            }
        })
    }

    private fun setUpMap(map: GoogleMap?) {
        if(latitude!=null && longitude!=null ) {
            val location = LatLng(latitude!!, longitude!!)
            map?.animateCamera(CameraUpdateFactory.newLatLngZoom(location, 13f))
            wasZoomedIn=true
        }
        //map?.setPadding(0, convertDpToPx(200), 0, 0)
    }

//    private fun convertDpToPx(dp: Int): Int {
//        val r = getResources()
//        val px = TypedValue.applyDimension(
//                TypedValue.COMPLEX_UNIT_DIP,
//                dp.toFloat(),
//                r.getDisplayMetrics()
//        )
//        return px.toInt()
//    }

    private fun searchForToilets(query: String) {

        apiServices.getMatches(query).enqueue(object : Callback<List<ToiletEntity>> {
            override fun onResponse(call: Call<List<ToiletEntity>>, response: Response<List<ToiletEntity>>) {
                //searchToilets = response.body()
                removeMarkers()
                findViewById<View>(R.id.bottom_sheet).visibility = View.GONE
                addMarkersToMap(response.body()!!, map)
                moveToClosest()
            }

            override fun onFailure(call: Call<List<ToiletEntity>>, t: Throwable) {
                Toast.makeText(this@MainActivity, "Search failed", Toast.LENGTH_SHORT).show()
            }

        })
    }

    private fun addMarkersToMap(input: List<ToiletEntity>, map: GoogleMap?) {
        /*val temp: List<ToiletEntity.kt> = nearbyToilets!!
        for (element in temp) {
            val marker = map?.addMarker(MarkerOptions().title(element.name).snippet(element.directions).position(LatLng(element.latitude.toDouble(), element.longitude.toDouble())))
            if (!markerData?.contains(marker)!!) {
                markerData?.set(marker!!, element)
            }
        }*/

        for (element in input) {
            val marker = map?.addMarker(MarkerOptions()
                    .title(element.name)
                    .snippet(element.directions)
                    .position(LatLng(element.latitude.toDouble(), element.longitude.toDouble())))
            if (!markerData?.contains(marker)!!) {
                markerData?.set(marker!!, element)
            }
        }

        Toast.makeText(this, "Toilets added to map", Toast.LENGTH_SHORT).show()
    }

    override fun onInfoWindowClick(marker: Marker?) {
        findViewById<View>(R.id.bottom_sheet).visibility = View.VISIBLE
        //binding.toilet = markerData?.get(marker)
        var toilet = markerData?.get(marker)
        findViewById<TextView>(R.id.bottom_sheet_head).text = toilet?.name
        findViewById<TextView>(R.id.bottom_sheet_directions).text = toilet?.directions
        findViewById<TextView>(R.id.bottom_sheet_accessible).text = if (toilet?.accessible!!) {
            "Yes"
        } else {
            "No"
        }
        findViewById<TextView>(R.id.bottom_sheet_changing_table).text = if (toilet.changing_table) {
            "Yes"
        } else {
            "No"
        }
        findViewById<TextView>(R.id.bottom_sheet_unisex).text = if (toilet.unisex) {
            "Yes"
        } else {
            "No"
        }
        findViewById<TextView>(R.id.bottom_sheet_comment).text = toilet.comment
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
    }

    private fun setUpLocationUpdates() {
        builder = LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest)
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult ?: return
                for (location in locationResult.locations) {
                    //Log.d("TAG", "Location updated")
                    latitude = location.latitude
                    longitude = location.longitude
                    if(!wasZoomedIn)
                    {
                        val location = LatLng(latitude!!, longitude!!)
                        map?.animateCamera(CameraUpdateFactory.newLatLngZoom(location, 13f))
                        wasZoomedIn=true
                    }
                }
            }
        }

        val client: SettingsClient = LocationServices.getSettingsClient(this)
        val task: Task<LocationSettingsResponse> = client.checkLocationSettings(builder.build())
        task.addOnSuccessListener { locationSettingsResponse ->
            startLocationUpdates()
        }

        task.addOnFailureListener { exception ->
            if (exception is ResolvableApiException) {
                // Location settings are not satisfied, but this can be fixed
                // by showing the user a dialog.
                try {
                    // Show the dialog by calling startResolutionForResult(),
                    // and check the result in onActivityResult().
                    exception.startResolutionForResult(this@MainActivity,
                            REQUEST_CHECK_SETTINGS)
                } catch (sendEx: IntentSender.SendIntentException) {
                    // Ignore the error.
                }
            }
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            REQUEST_CHECK_SETTINGS -> {
                when (resultCode) {
                    Activity.RESULT_CANCELED -> {
                     AlertDialog.Builder(this)
                        .setTitle("GPS Needed")
                        .setMessage("This app needs the GPS to be turned on in order to find nearest toilet")
                        .setPositiveButton("OK", object : DialogInterface.OnClickListener {
                            override fun onClick(p0: DialogInterface?, p1: Int) {
                              //  setUpLocationUpdates()
                            }

                        })
                        .create()
                        .show()
                    }
                }

            }

        }
    }

    private fun startLocationUpdates() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && !isUpdatingLocation) {
            fusedLocationClient.requestLocationUpdates(locationRequest,
                    locationCallback,
                    null /* Looper */)
            isUpdatingLocation = true
        }
    }

    override fun onSaveInstanceState(outState: Bundle?, outPersistentState: PersistableBundle?) {
        super.onSaveInstanceState(outState, outPersistentState)
    }

    private fun removeMarkers() {
        markerData?.forEach { entry -> entry.key.remove() }
        markerData = mutableMapOf()
    }

    private fun moveToClosest() {
        var currentMinDistance: Double = 1000.0
        lateinit var locationToCompare: LatLng
        lateinit var closestMarker: Marker
        if (longitude != null && latitude != null) {
            markerData?.forEach { entry ->
                run {
                    locationToCompare = entry.key.position
                    //Log.d("TAG", (latitude==null).toString()    )
                    var currentDistance = calculateDistance(locationToCompare, LatLng(latitude!!, longitude!!))
                    if (currentDistance < currentMinDistance) {
                        currentMinDistance = currentDistance
                        closestMarker = entry.key
                    }
                }
            }
            //val tmp : LatLng=LatLng(closestMarker.position.latitude,closestMarker.position.longitude)
            map?.animateCamera(CameraUpdateFactory.newLatLngZoom(closestMarker.position, 13f))
        }
    }

    private fun calculateDistance(entry1: LatLng, entry2: LatLng): Double {
        return Math.sqrt(Math.pow((entry1.latitude - entry2.latitude), 2.0) + Math.pow((entry1.longitude - entry2.longitude), 2.0))
    }

    private fun stopLocationUpdates() {
        fusedLocationClient.removeLocationUpdates(locationCallback)
        isUpdatingLocation = false
    }

    override fun onPause() {
        super.onPause()
        stopLocationUpdates()
    }
}



