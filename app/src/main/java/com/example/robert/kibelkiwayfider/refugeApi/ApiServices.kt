package com.example.robert.kibelkiwayfider.refugeApi

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query


interface ApiServices {
    @GET("v1/restrooms/search.json")
    fun getMatches(@Query("query") query: String): Call<List<ToiletEntity>>

    @GET("v1/restrooms/by_location.json")
    fun getNearby(@Query("latitude") latitude: Float, @Query("longitude") longitude: Float): Call<List<ToiletEntity>>

}