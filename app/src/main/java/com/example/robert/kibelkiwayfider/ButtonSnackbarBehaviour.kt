package com.example.robert.kibelkiwayfider

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.Snackbar
import android.support.v4.view.ViewCompat
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.view.ViewTreeObserver
import android.widget.Button


class ButtonSnackbarBehaviour : CoordinatorLayout.Behavior<Button>, ViewTreeObserver.OnDrawListener {
    constructor() : super()
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    var childY: Float = 0f

    override fun layoutDependsOn(parent: CoordinatorLayout, child: Button, dependency: View): Boolean {
        return dependency is ConstraintLayout
    }

    override fun onDraw() {
    }

    override fun onDependentViewChanged(parent: CoordinatorLayout, child: Button, dependency: View): Boolean {
        if (childY == 0f) {
            mySetChildY(child.y)
        }
        //Float childY = child.getY()
        //  val displayMetrics = parent.getResources().getDisplayMetrics()
        child.y = childY + getFabTranslationYForSnackbar(parent, child)
        Log.d("Child y: ", childY.toString())
        return false
    }

    private fun getFabTranslationYForSnackbar(parent: CoordinatorLayout,
                                              button: Button): Float {
        var minOffset = 0f
        val dependencies = parent.getDependencies(button)
        var i = 0
        val z = dependencies.size
        //Log.d("Dependencies size: ", z.toString())
        while (i < z) {
            val view = dependencies[i]
            if (view is Snackbar.SnackbarLayout && parent.doViewsOverlap(button, view)) {
                minOffset = Math.min(minOffset,
                        ViewCompat.getTranslationY(view) - view.getHeight())
            }
            i++
            Log.d("Current Offset:", minOffset.toString())
        }

        return minOffset
    }

    fun mySetChildY(input: Float) {
        childY = input
    }


}